export default {
  app: {
    // Actions
    FETCH_PRODUCTS: 'FETCH_PRODUCTS',
    NAVIGATE: 'NAVIGATE',
    ADD_TO_CART: 'ADD_TO_CART',
    REMOVE_FROM_CART: 'REMOVE_FROM_CART',
    CHECKOUT: 'CHECKOUT',

    // Views
    INDEX_VIEW: 'INDEX',
    DETAIL_VIEW: 'DETAIL',
    CART_VIEW: 'CART'
  },

  styles: {
    lightContrast: '#DDDDDD',
    medContrast: '#888888',
    darkContrast: '#222222',
    primaryHighlight: '#F95658',
  }
}
