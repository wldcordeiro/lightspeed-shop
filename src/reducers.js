import constants from './constants';

function initState() {
  return {
    productData: {
      status: 'pending',
      products: [],
      stock: {},
    },
    cart: {},
    cartCount: 0,
    view: constants.app.INDEX_VIEW,
    id: '',
  }
}

export function appReducer(state = initState(), action) {
  switch (action.type) {
  case constants.app.NAVIGATE:
    if (action.view === constants.app.DETAIL_VIEW) {
      history.pushState({
        view: action.view,
        id: action.id
      }, "", `#/${action.view.toLowerCase()}/${action.id}`);
      sessionStorage.setItem('view', action.view);
      sessionStorage.setItem('id', action.id);
      return {...state, view: action.view, id: action.id };
    }

    if (action.view === constants.app.INDEX_VIEW) {
      history.pushState({ view: action.view }, "", "#/");
      sessionStorage.setItem('view', action.view);
    } else {
      history.pushState({ view: action.view }, "", `#/${action.view.toLowerCase()}`);
      sessionStorage.setItem('view', action.view);
    }

    return {...state, view: action.view};
  case constants.app.ADD_TO_CART:
    if (state.cart.hasOwnProperty(action.id)) {
      const newCart = { ...state.cart, [action.id]: state.cart[action.id] += 1 };
      const cartCount = Object
        .values(newCart).reduce((acc, i) => acc += i, 0);
      const newStock = {...state.productData.stock};
      newStock[action.id] -= 1;
      return {
        ...state,
        cart: newCart,
        cartCount,
        productData: { ...state.productData, stock: newStock }
      };
    }
    const newCart = { ...state.cart, [action.id]: 1 };
    const cartCount = Object.values(newCart).reduce((acc, i) => acc += i, 0);
    const newStock = {...state.productData.stock};
    newStock[action.id] -= 1;
    return {
      ...state,
      cart: newCart,
      cartCount,
      productData: { ...state.productData, stock: newStock }
    };
  case constants.app.REMOVE_FROM_CART:
    if (state.cart.hasOwnProperty(action.id)) {
      const newCart = {...state.cart};
      newCart[action.id] = newCart[action.id] -= 1
      const newStock = {...state.productData.stock};
      newStock[action.id] += 1;
      return {
        ...state,
        cart: newCart,
        cartCount: state.cartCount -= 1,
        productData: { ...state.productData, stock: newStock }
      };
    }

    return state;
  case constants.app.CHECKOUT:
    return { ...state, cart: {}, cartCount: 0 };
  case constants.app.FETCH_PRODUCTS:
    if (action.status === 'success') {
      const productStock = {};
      action.products
        .forEach(product => {
          if (!productStock.hasOwnProperty(product._id)) {
            productStock[product._id] = product.stock.remaining;
          }
        });

      return {...state, productData: {
        status: action.status,
        products: action.products
          .map(p => ({...p, stock: productStock[p._id]})),
        stock: productStock
      }};
    }

    if (action.status === 'error') {
      return {...state, productData: {
          status: action.status,
          products: action.products
      }};
    }

    return state;
  default:
    history.pushState({ view: constants.app.INDEX_VIEW }, "", "#/");
    return state;
  }
}

export function getSelectedProduct(state, id) {
  return state.productData.products.find(p => p._id === id);
}

export function getSelectedProducts(state, ids) {
  return state.productData.products.filter(p => ids.includes(p._id));
}
