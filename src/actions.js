import constants from './constants';

export function fetchProducts() {
  return (dispatch) => {
    dispatch({ type: constants.app.FETCH_PRODUCTS });
    fetch('http://beta.json-generator.com/api/json/get/4kiDK7gxZ')
    .then(response => response.json())
    .then(products => {
      dispatch({
        type: constants.app.FETCH_PRODUCTS,
        status: 'success',
        products
      });
    })
    .catch(err => {
      console.log(err);
      dispatch({
        type: constants.app.FETCH_PRODUCTS,
        status: 'error',
        products: []
      });
    });
  };
}

export function navigateTo(view, id) {
  return {
    type: constants.app.NAVIGATE,
    view,
    id
  };
}

export function addToCart(id) {
  return {
    type: constants.app.ADD_TO_CART,
    id
  };
}

export function removeFromCart(id) {
  return { type: constants.app.REMOVE_FROM_CART, id };
}

export function checkout() {
  return {
    type: constants.app.CHECKOUT,
  };
}
