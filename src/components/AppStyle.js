import constants from '../constants';

export default {
  '@global': {
    ':root': {
      fontFamily: "'Lato', sans-serif",
      fontSize: 16
    },
    html: {
      boxSizing: 'border-box'
    },
    '*, *:before, *:after': {
      boxSizing: 'inherit'
    },
    body: {
      margin: 0,
      padding: 0,
      backgroundColor: constants.styles.medContrast
    }
  },
  App: {
    width: '80vw',
    height: '100%',
    margin: '0 auto'
  },
  siteHead: {
    backgroundColor: constants.styles.darkContrast,
    display: 'flex',
    justifyContent: 'flext-start',
    alignItems: 'center',
    padding: '0 4rem',
    '@global': {
      h1: {
        color: constants.styles.primaryHighlight,
        fontFamily: "'Passion One', cursive",
        fontSize: "calc((0.5rem + 1.5vh + 1.5vw))",
        margin: '0 1rem'
      }
    }
  },
  siteNav: {
    '@global': {
      ul: {
        listStyleType: 'none',
        padding: 0
      }
    },
    marginLeft: 'auto',
  },
  cartBtn: {
    position: 'relative',
    background: 'none',
    border: 'none',
    padding: 0,
    '@global': {
      img: {
        height: '2.5rem',
      }
    },

  },
  cartCount: {
    position: 'absolute',
    top: 0,
    left: 0,
    padding: '0.25rem',
    backgroundColor: constants.styles.primaryHighlight,
    fontSize: '0.75rem',
    borderRadius: '8px'
  },
  '@media (max-device-width : 480px)': {
    siteHead: {
      padding: '0 1rem'
    }
  }
};
