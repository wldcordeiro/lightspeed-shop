import constants from '../constants';
import ctaButton from './ctaButton';

export default {
  productCard: {
    display: 'flex',
    flexWrap: 'wrap',
    margin: '1.5rem',
    padding: '1rem',
    height: '20rem',
    width: '20rem',
    backgroundColor: constants.styles.darkContrast,
    color: constants.styles.medContrast,
    '@global': {
      img: {
        height: '60%',
        width: '100%'
      },
      h2: {
        fontSize: "calc((0.5rem + 1.0vh + 1.2vw))",
        width: '100%',
        margin: 0
      },
      p: {
        display: 'inline-flex',
        alignItems: 'center',
        width: '30%',
        margin: 0
      },
      span: {
        display: 'inline-block',
        height: '1rem',
        width: '1rem',
        margin: '0 1rem'
      }
    },
  },
  ctaButton: {
    ...ctaButton
  },
  '@media (max-device-width : 480px)': {
    productCard: {
      margin: '1rem',
      height: '16rem',
      width: '16rem',
    }
  }
};
