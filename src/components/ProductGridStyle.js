import ContentStyle from './ContentStyle';

export default {
  productSection: {
    ...ContentStyle
  },
  productGrid: {
    justifyContent: 'center',
    flexWrap: 'wrap',
  },
  productError: {

  },
  productLoading: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    '@global': {
      svg: {
        width: '6rem',
        height: '6rem',
        animation: 'spin 4s linear infinite'
      }
    }
  },
  '@keyframes spin': {
    '100%': {
      transform: 'rotate(360deg)'
    }
  }
};
