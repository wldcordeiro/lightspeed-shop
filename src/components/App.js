import React, { Component } from 'react';
import injectSheet from 'react-jss';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import AppStyle from './AppStyle';
import constants from '../constants';
import * as actions from '../actions';
import ProductGrid from './ProductGrid';
import ProductDetail from './ProductDetail';
import ShoppingCart from './ShoppingCart';
import cart from './cart.png';

class App extends Component {
  componentDidMount() {
    const { fetchProducts, productData } = this.props;
    if (productData.status === 'pending') {
      fetchProducts();
    }
  }

  renderContentSection() {
    const { view } = this.props;
    if (view === constants.app.DETAIL_VIEW) {
      return <ProductDetail />
    }

    if (view === constants.app.CART_VIEW) {
      return <ShoppingCart />
    }

    return <ProductGrid />
  }

  renderCartCount() {
    const { cartCount, classes } = this.props;
    if (cartCount > 0) {
      return <span className={classes.cartCount}>{cartCount}</span>;
    }
    return;
  }

  render() {
    const { navigateTo, classes } = this.props;
    return <main className={classes.App}>
      <header className={classes.siteHead}>
        <h1 onClick={
          navigateTo.bind(null, constants.app.INDEX_VIEW)}>LightSpeed Shop</h1>
        <nav className={classes.siteNav}>
          <ul>
            <li>
              <button className={classes.cartBtn}
                onClick={navigateTo.bind(null, constants.app.CART_VIEW)}>
                <img src={cart} alt="Shopping Cart"/>
                {this.renderCartCount()}
              </button>
            </li>
          </ul>
        </nav>
      </header>
      {this.renderContentSection()}
    </main>
  }
}

App.propTypes = {
  fetchProducts: React.PropTypes.func.isRequired,
  navigateTo: React.PropTypes.func.isRequired,
  cartCount: React.PropTypes.number.isRequired,
  productData: React.PropTypes.shape({
    products: React.PropTypes.array.isRequired,
    status: React.PropTypes.string.isRequired,
    stock: React.PropTypes.object.isRequired
  }),
};

export default injectSheet(AppStyle)(connect(
  state => state,
  dispatch => bindActionCreators(actions, dispatch)
)(App));
