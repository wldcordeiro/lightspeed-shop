import constants from '../constants';
import ContentStyle from './ContentStyle';
import ctaButton from './ctaButton';

export default {
  cartSection: {
    ...ContentStyle,
    flexWrap: 'wrap',

    '@global': {
      ul: {
        listStyleType: 'none',
        padding: 0,
        width: '100%',
        height: '100%'
      }
    }
  },
  cartHead: {
    width: '100%',
    height: '2rem'
  },
  cartItem: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'flex-start',
    alignItems: 'center',
    margin: '2rem 0',
  },
  cartImg: {
    height: '20rem',
    width: '20rem',
    marginRight: 'auto'
  },
  cartItemInfo: {
    display: 'flex',
    flexDirection: 'column',
    marginLeft: 'auto',
    '@global': {
      p: {
        width: '100%',
        margin: '0.5rem'
      },
      h2: {
        width: '100%',
        margin: '0.5rem'
      },
      span: {
        display: 'inline-block',
        height: '1rem',
        width: '1rem',
        margin: '0 1rem'
      },

      button: {
        padding: '1rem',
        backgroundColor: 'white',
        border: 'none',

        '&:hover': {
          boxShadow: '0 0 0.25rem black'
        }
      }
    },
  },
  ctaButton: {
    ...ctaButton,
  },
  cartTotal: {
    width: '100%',
  },
  modalWrap: {
    position: 'fixed',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100vw',
    height: '100vh',
    top: 0,
    left: 0,
    zIndex: 99999,
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  modalHead: {
    height: '40%'
  },

  modalArticle: {
    width: '50%',
    height: '10rem',
    backgroundColor: constants.styles.lightContrast,
    color: constants.styles.darkContrast,
    padding: '2rem'
  },
  modalCta: {
    ...ctaButton
  },
  '@media (max-device-width : 480px)': {
    cartItem: {
      margin: '1rem 0',
    },
    cartImg: {
      height: '12rem',
      width: '100%',
    },
    cartItemInfo: {
      width: '100%',
    },
    modalArticle: {
      width: '90%',
      height: '30rem',
    },
    modalHead: {
      height: '80%'
    },
    modalCta: {
      alignSelf: 'flex-end',
    }
  }
};
