import React, { Component } from 'react';
import classnames from 'classnames';
import injectSheet from 'react-jss';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Svg from 'react-inlinesvg';

import ProductCard from './ProductCard';
import ProductGridStyle from './ProductGridStyle';
import * as actions from '../actions';
import constants from '../constants';
import loading from './loading.svg';

class ProductGrid extends Component {
  render() {
    const { classes, products, status, stock, navigateTo, addToCart } = this.props;

    if (status === 'error') {
      return (
        <section
          className={classnames(classes.productSection, classes.productError)}>
          <h1>Something went wrong fetching products... :(</h1>
        </section>
      );
    }

    if (status === 'success') {
      return (
        <section
          className={classnames(classes.productSection, classes.productGrid)}>
          {
            products.map(({ color, image, price, _id }) =>
            <ProductCard
              handleClick={
                navigateTo.bind(null, constants.app.DETAIL_VIEW, _id)}
              handleAdd={
                stock[_id] > 0 ?addToCart.bind(null, _id) : null
              }
              color={color}
              image={image}
              price={price}
              _id={_id}
              key={_id}
            />)
          }
        </section>
      );
    }

    return (
      <section
        className={classnames(classes.productSection, classes.productLoading)}>
        <Svg src={loading} />
      </section>
    );
  }
};

ProductGrid.propTypes = {
  products: React.PropTypes.array.isRequired,
  status: React.PropTypes.oneOf(['pending', 'success', 'error']).isRequired,
  classes: React.PropTypes.object.isRequired,
}

export default injectSheet(ProductGridStyle)(connect(
  state => state.productData,
  dispatch => bindActionCreators(actions, dispatch)
)(ProductGrid));
