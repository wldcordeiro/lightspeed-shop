import ContentStyle from './ContentStyle';
import ctaButton from './ctaButton';

export default {
  detailSection: {
    ...ContentStyle,
    flexWrap: 'wrap',

  },
  productImg: {
    width: '33%',
    height: '25rem',
    margin: '0 1rem'
  },
  productColor: {
    display: 'inline-block',
    height: '1rem',
    width: '1rem',
    margin: '0 1rem'
  },
  description: {
    width: '62%',
    margin: '1rem 0',
  },
  productInfo: {
    display: 'flex',
    flexDirection: 'column',
    marginRight: 'auto',
  },
  ctaButton: {
    ...ctaButton,
    height: '3rem',
    width: '12rem',
    fontSize: '1.5rem',
  },

  '@media (max-device-width : 480px)': {
    productImg: {
      width: '100%',
      height: '16rem',
      margin: '1rem 0',
    },
    description: {
      width: '100%',
    },
    productInfo: {
      '@global': {
        'h1, h2': {
          margin: 0
        }
      }
    },
    ctaButton: {
      width: '100%',
    }
  }
};
