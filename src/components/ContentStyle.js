import constants from '../constants';

export default {
  backgroundColor: constants.styles.lightContrast,
  boxShadow: '0 0 1rem black',
  padding: '2rem',
  display: 'flex',
  minHeight: '90vh',
  height: '100%'
};
