import React, { Component } from 'react';
import injectSheet from 'react-jss';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from '../actions';

import ShoppingCartStyle from './ShoppingCartStyle';
import { getSelectedProducts } from '../reducers';

class ShoppingCart extends Component {
  constructor(props) {
    super(props);
    this.state = { orderSubmitted: false };
    this.handleCheckout = this.handleCheckout.bind(this);
  }
  renderCartList() {
    const { addToCart, removeFromCart, classes } = this.props;
    if (this.props.cartCount > 0) {
      return this.props.productList.map(product => {
        return (
          <li key={product._id} className={classes.cartItem}>
            <img className={classes.cartImg} src={product.image} alt="Product" />
            <div className={classes.cartItemInfo}>
              <p>
                Color:
                <span style={{ backgroundColor: product.color }}></span>
              </p>
              <p>Stock: {this.props.productData.stock[product._id]}</p>
              <h2>Price: {product.price}</h2>
              <div>
                {'Quantity: '}
                <button onClick={
                  this.props.productData.stock[product._id] > 0 ?
                  addToCart.bind(null, product._id) : null }>▲</button>
                  <span>{this.props.cart[product._id]}</span>
                <button
                  onClick={removeFromCart.bind(null, product._id)}>▼</button>
              </div>
            </div>
          </li>
        );
      });
    }

    return <li><h2>Nothing in cart.</h2></li>
  }

  handleCheckout() {
    this.setState({ orderSubmitted: true });
    this.props.checkout();
  }

  renderTotal() {
    const { classes, cart, productList } = this.props;
    const total = productList.reduce((acc, product) => {
      const priceAsNum = product.price.slice(1).replace(',', '');
      const totalProductPrice = priceAsNum * cart[product._id];
      return acc += totalProductPrice;
    }, 0);
    if (total > 0) {
      return (
        <div className={classes.cartTotal}>
          <h2>Sub-Total</h2>
          <h3>{parseFloat(total).toFixed(2)}</h3>
          <button
            className={classes.ctaButton}
            onClick={this.handleCheckout}>Checkout</button>
        </div>
      );
    }
  }

  renderCheckoutModal() {
    if (this.state.orderSubmitted) {
      return (
        <div className={this.props.classes.modalWrap}>
          <article className={this.props.classes.modalArticle}>
            <h1 className={this.props.classes.modalHead}>Order successfully submitted!</h1>
            <button className={this.props.classes.modalCta}
              onClick={() => this.setState({ orderSubmitted: false })}>
              Dismiss</button>
          </article>
        </div>
      );
    }
  }

  render() {
    const { classes } = this.props;
    return (
      <section className={classes.cartSection}>
        <h1 className={classes.cartHead}>Shopping Cart</h1>
        <ul>
          {this.renderCartList()}
        </ul>
        {this.renderTotal()}
        {this.renderCheckoutModal()}
      </section>
    );
  }
};

export default injectSheet(ShoppingCartStyle)(connect(
  state => ({...state, productList: getSelectedProducts(state, Object.keys(state.cart)) }),
  dispatch => bindActionCreators(actions, dispatch)
)(ShoppingCart));
