import React from 'react';
import injectSheet from 'react-jss';

import ProductCardStyle from './ProductCardStyle';

function ProductCard({
  color, image, price, _id, classes, handleAdd, handleClick }) {
  return (
    <article className={classes.productCard}>
      <img src={image} alt="Product" onClick={handleClick}/>
      <h2>{price}</h2>
      <p>
        Color:
        <span style={{ backgroundColor: color }}></span>
      </p>
      <button className={classes.ctaButton} onClick={handleAdd}>Add to Cart</button>
    </article>
  );
}

export default injectSheet(ProductCardStyle)(ProductCard)
