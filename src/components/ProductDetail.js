import React, { Component } from 'react';
import injectSheet from 'react-jss';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from '../actions';
import { getSelectedProduct } from '../reducers';

import ProductDetailStyle from './ProductDetailStyle';

class ProductDetail extends Component {
  render() {
    const {
      classes,
      addToCart,
      product: {_id, color, image, description, price },
      productData: { stock }
    } = this.props;
    return (
      <section className={classes.detailSection}>
          <img className={classes.productImg} src={image} alt="Product"/>
          <p className={classes.description}>{description}</p>
          <div className={classes.productInfo}>
            <h2>{price}</h2>
            <h3>{stock[_id]} in stock</h3>
            <p>
              Color:
              <span className={classes.productColor}style={{ backgroundColor: color }}></span>
            </p>
          </div>
          <button
            className={classes.ctaButton}
            onClick={stock[_id] > 0 ? addToCart.bind(null, _id) : null}>Add to Cart</button>
      </section>
    );
  }
};

export default injectSheet(ProductDetailStyle)(connect(
  state => ({...state, product: getSelectedProduct(state, state.id) }),
  dispatch => bindActionCreators(actions, dispatch)
)(ProductDetail));
