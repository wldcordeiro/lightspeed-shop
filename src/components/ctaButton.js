import constants from '../constants';

export default {
  fontSize: '0.75rem',
  backgroundColor: constants.styles.primaryHighlight,
  color: constants.styles.darkContrast,
  border: 'none',
  height: '1.5rem',
  width: '6rem',
  marginLeft: 'auto',
  alignSelf: 'center',
  '&:hover': {
    boxShadow: '0 0 0.25rem white',
  }
};
