/*
 * fetch - A function that returns an XMLHttpRequest wrapped in a Promise
 *
 * It takes a URL and makes a GET request to that URL.
 */
export function fetch(url) {
  return new Promise((resolve, reject) => {
    const request = new XMLHttpRequest();
    request.open('GET', url);

    request.onload = () => {
      if (request.status == 200) {
        resolve(request.response);
      } else {
        reject(Error(request.statusText));
      }
    };

    request.onerror = () => {
      reject(Error('Network Error!'));
    };

    request.send();
  });
}
