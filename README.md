# LightSpeed Shop

This is the LightSpeed Shop app made for the technical exercise provided by LightSpeed HQ. It's a simple application built with React and Redux.

The reasons for utilizing React and Redux are as follows.

## React

React is a very powerful view library that allows you to write component based UIs easily and lends itself well to proto-typing and iterative design.

## Redux

Redux simplifies state management by making all state come from a single immutable source that you act upon by firing actions and having "reducer" handler functions that manipulate the state for each action type.

## JSS

JSS was chosen for style management as it integrates very well with React, produces namespaced class names for your components and provides a robust API for simplifying and reusing style code.

It utilizes `create-react-app`:

To run locally:

```sh
yarn install # Or npm install
yarn start # Or npm start
```
and then go to `localhost:3000`

To build just run

```sh
yarn build # Or npm run build
```
